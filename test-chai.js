const chai = require('chai');
const expect = chai.expect;
const math = require('./math');

describe('Test chai ', ()=> {
    it('should compare thing by expect', ()=> {
         expect(1).to.equal(1);
    });
    it('should compare another things by expect ',()=>{
        expect(5>8).to.be.false;
        expect({name: 'piano'}).to.deep.equal({name: 'piano'});
        expect({name: 'piano'}).to.have.property('name').to.equal('piano');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('piano').to.be.a('string');
        expect('piano'.length).to.equal(5);
        expect('piano').to.lengthOf(5);
        expect([1,2,3]).to.lengthOf(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;

    });
});

describe('Math module', ()=> {
   context('function add1', ()=>{
   it('ควรส่งค่ากลับเป็นตัวเลข', ()=>{
       expect(math.add1(0,0)).to.be.a('number');

      });
      it('math.add1(1,1) ควรส่งค่ากลับเป็น 2', ()=>{
       expect(math.add1(1,1)).to.equal(2);
    });
   });
});